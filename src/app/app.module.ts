import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule, RoutingComponent } from './app.routing';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SummaryComponent } from './summary/summary.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { SettingsComponent } from './settings/settings.component';
import { WalletComponent } from './wallet/wallet.component';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    RoutingComponent,
    AppComponent,
    SidebarComponent,
    SummaryComponent,
    PortfolioComponent,
    ExchangeComponent,
    SettingsComponent,
    WalletComponent,
    HelpComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
